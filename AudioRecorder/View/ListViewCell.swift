//
//  ListViewCell.swift
//  AudioRecorder
//
//  Created by Andrei on 11/6/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class ListViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var nameRecord: UILabel!
    @IBOutlet weak var dateRecord: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        if selected {
//            self.layer.backgroundColor = UIColor(white: 1, alpha: 0.3).cgColor
//        } else {
//            self.layer.backgroundColor = UIColor.clear.cgColor
//        }
    }
    
    func updateViews(record: AudioRecorder, atIndex index: Int) {
        nameRecord.text = "\(record.nameStr!) \(index)"
        dateRecord.text = record.time
    }
}
