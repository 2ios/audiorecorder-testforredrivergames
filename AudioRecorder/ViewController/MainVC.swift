//
//  ViewController.swift
//  AudioRecorder
//
//  Created by Andrei on 11/6/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

class MainVC: UIViewController, AVAudioRecorderDelegate {
    
    //variables
    var second = 0
    var minute = 0
    var hour = 0
    var timer: Timer?
    var record: AudioRecorder?
    var startTime: String!
    var recordSession: AVAudioSession!
    var audioRecord: AVAudioRecorder!
    var isAllowed = false
    var uuid : String!
    
    //outlets
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var minuteLbl: UILabel!
    @IBOutlet weak var secondLbl: UILabel!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var recordListBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    // set status Bar Light for this View
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func initSetup() {
        //set time
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let time = formatter.string(from: date)
        timeLbl.text = time
        recordListBtn.layer.cornerRadius = 5
        recordListBtn.layer.borderWidth = 1
        recordListBtn.layer.borderColor = UIColor.white.cgColor
        
        // set permission for record
        recordSession = AVAudioSession.sharedInstance()
        do {
            try recordSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordSession.setActive(true)
            recordSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAllowed = true
                    } else {
                        print("Failed to allow")
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    // actions
    @IBAction func recordBtnWasPressed(_ sender: UIButton) {
        if isAllowed {
            recordBtn.isEnabled = false
            stopBtn.isEnabled = true
            let image = UIImage(named: "rec_on_btn")
            recordBtn.setImage(image, for: .disabled)
            
            statusLbl.text = "Record"
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy hh:mm:ss"
            startTime = formatter.string(from: date)
            
            //set timer
            timer = Timer()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            
            // generate name of record
            uuid = UUID.init().uuidString

            let audioFilename = getDocumentsDirectory().appendingPathComponent("\(uuid).m4a")
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]
            do {
                audioRecord = try AVAudioRecorder(url: audioFilename, settings: settings)
                audioRecord.delegate = self
                audioRecord.record()
            } catch {
                print(error)
            }
            addNewElementToCoreData()
        } else {
            let alert = UIAlertController(title: "Sorry", message: "You need to allowed to record in settings phone", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func stopBtnWasPressed(_ sender: UIButton) {
        if audioRecord != nil {
            stopBtn.isEnabled = false
            recordBtn.isEnabled = true
            let image = UIImage(named: "rec_off_btn")
            recordBtn.setImage(image, for: .normal)
            
            statusLbl.text = ""
            
            timer?.invalidate()
            audioRecord.stop()
            audioRecord = nil
            resetTimer()
            if record != nil {
                record!.nameStr = "Record"
                record!.time = startTime
                record!.recordPath = "\(uuid).m4a"
            }
            let app = UIApplication.shared.delegate as! AppDelegate
            app.saveContext()
        }
    }
    
    @IBAction func listBtnWasPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let listVC = storyboard.instantiateViewController(withIdentifier: "ListVC")
        self.present(listVC, animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func addNewElementToCoreData() {
        let app = UIApplication.shared.delegate as! AppDelegate
        record = NSEntityDescription.insertNewObject(forEntityName: "AudioRecorder", into: app.persistentContainer.viewContext) as? AudioRecorder
    }
    
    func updateTimer() {
        second += 1
        if second == 60 {
            second = 0
            minute += 1
        }
        if minute == 60 {
            minute = 0
            hour += 1
        }
        hourLbl.text = hour > 9 ? String(describing: hour): String(describing: "0\(hour)")
        minuteLbl.text = minute > 9 ? String(describing: minute):  String(describing: "0\(minute)")
        secondLbl.text = second > 9 ? String(describing: second): String(describing: "0\(second)")
    }
    
    func resetTimer() {
        second = 0
        minute = 0
        hour = 0
        hourLbl.text = "00"
        minuteLbl.text = "00"
        secondLbl.text = "00"
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            print("Succes record")
        }
    }
}

