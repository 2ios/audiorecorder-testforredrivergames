//
//  ListVC.swift
//  AudioRecorder
//
//  Created by Andrei on 11/6/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

class ListVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, AVAudioPlayerDelegate {
    
    //variables
    var arrayRecord = [AudioRecorder]()
    var second = 0
    var minute = 0
    var hour = 0
    var timer: Timer?
    var player: AVAudioPlayer? = nil
    var selectedRecord = 0
    
    //outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRecordsView: UIView!
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var minuteLbl: UILabel!
    @IBOutlet weak var secondLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        getContext()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkNoRecords()
    }
    
    // set status Bar Light for this View
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    // actions
    @IBAction func playBtnWasPressed(_ sender: UIButton) {
        if selectedRecord < arrayRecord.count {
            playRecord(atIndex: selectedRecord)
        }
    }
    
    @IBAction func stopBtnWasPressed(_ sender: UIButton) {
        if player != nil {
            resetTimer()
            player?.stop()
            player?.currentTime = 0
            player = nil
        }
    }

    @IBAction func deleteBtnWasPressed(_ sender: UIButton) {
        tableView.setEditing(true, animated: true)
        if player != nil {
            resetTimer()
            player?.stop()
            player?.currentTime = 0
            player = nil
        }
    }
    
    @IBAction func cancelBtnWasPressed(_ sender: UIButton) {
        tableView.setEditing(false, animated: true)
    }
    
    @IBAction func navBackBtnPressed(_ sender: Any) {
        if player != nil {
            resetTimer()
            player?.stop()
            player?.currentTime = 0
            player = nil
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func playRecord(atIndex: Int) {
        resetTimer()
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        } catch {
            print(error)
            print("trecut pe aici")
        }
        
        do {
            player = try AVAudioPlayer(contentsOf: getURL(atIndex: atIndex))
            player?.delegate = self
            player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func resetTimer() {
        timer?.invalidate()
        second = 0
        minute = 0
        hour = 0
        hourLbl.text = "00"
        minuteLbl.text = "00"
        secondLbl.text = "00"
    }
    
    func updateTimer() {
        second += 1
        if second == 60 {
            second = 0
            minute += 1
        }
        if minute == 60 {
            minute = 0
            hour += 1
        }
        hourLbl.text = hour > 9 ? String(describing: hour): String(describing: "0\(hour)")
        minuteLbl.text = minute > 9 ? String(describing: minute):  String(describing: "0\(minute)")
        secondLbl.text = second > 9 ? String(describing: second): String(describing: "0\(second)")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRecord.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ListView") as? ListViewCell {
            let record = arrayRecord[indexPath.row]
            cell.updateViews(record: record, atIndex: indexPath.row)
            return cell
        } else {
            return ListViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRecord = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            do {
                try FileManager.default.removeItem(at: getURL(atIndex: selectedRecord))
            } catch {
                print(error)
            }
            let app = UIApplication.shared.delegate as! AppDelegate
            app.persistentContainer.viewContext.delete(arrayRecord[selectedRecord])
            app.saveContext()
            arrayRecord.remove(at: selectedRecord)
            tableView.deleteRows(at: [IndexPath(row: selectedRecord, section: 0)], with: .fade)
            checkNoRecords()
            tableView.reloadData()
        }
    }
    
    func getURL(atIndex: Int) -> URL {
        let localPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let soundFileUrl = localPath.appendingPathComponent((arrayRecord[atIndex].recordPath)!)
        return soundFileUrl
    }
    
    func checkNoRecords() {
        if arrayRecord.count > 0 {
            noRecordsView.isHidden = true
        } else {
            noRecordsView.isHidden = false
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        resetTimer()
    }
    
    // MARK: - Core Data Get support
    func getContext() {
        let app = UIApplication.shared.delegate as! AppDelegate
        let fetchRequest = AudioRecorder.fetchRequest() as NSFetchRequest
        do {
            arrayRecord = try app.persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            print(error)
        }
    }
}
